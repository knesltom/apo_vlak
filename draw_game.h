
// #ifndef DRAW_GAME
// #define DRAW_GAME

#include "game_utils.h"

void drawBoard(tile *board, train_s *train, char **lootArray, char **trainImages);

void drawCrash(train_s *train, char **trainImages);

void drawTrain(train_s *train, tile *board, char **trainImages, int trainDirection);

void drawOpenGate(int gate, char **gameImages);

void eraseTrain(train_s *train, tile *board);

void drawGameAsset(char *lootArray, int startX, int startY);

void pickColors(char *asset, int startX, int startY);

// #endif