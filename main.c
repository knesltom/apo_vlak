#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/time.h>

#include <unistd.h>
#include "levels.h"

#define _POSIX_C_SOURCE 200112L

int redButton = 0;
int greenButton = 0;
int blueButton = 0;

pthread_mutex_t buttonMutex;

#define NUMBER_OF_LEVELS 3 // temporary

int main(void)
{

    if (initLCD() == false || initKNOBS() == false || initLEDS() == false)
    {
        fprintf(stderr, "Peripherals initialization failed!\n");
        exit(1);
    }

    char **trainImages = allocateAssetMemory(BLOCK_SIZE, 14);
    readAssets(trainImages, "train_assets.txt", 14);

    char **gameAssets = allocateAssetMemory(BLOCK_SIZE, 6);
    readAssets(gameAssets, "game_assets.txt", 6);

    tile *board = malloc(BOARD_SIZE * sizeof(tile));

    train_s *train = (train_s *)malloc(sizeof(train_s));
    allocateTrain(train);
    if (!trainImages || !gameAssets || !board || !train)
    {
        fprintf(stderr, "Closing program...\n");
        exit(1);
    }

    pthread_t buttonThread;

    pthread_mutex_init(&buttonMutex, NULL);

    pthread_create(&buttonThread, NULL, buttonPressThread, NULL);

    int levelChoice = playOpeningSequence("level_selection.txt", trainImages, gameAssets, train, board);
    if (levelChoice >= 1)
    {
        int gatePosition = loadLevelFromFile(board, "level_selection.txt", train, levelChoice);
        runLevel(gatePosition, board, train, trainImages, gameAssets, levelChoice, 1);
    }
    else if (levelChoice == 0)
    {
        int handChoice = showLevels();
        int gatePosition = loadLevelFromFile(board, "level_selection.txt", train, handChoice);
        runLevel(gatePosition, board, train, trainImages, gameAssets, handChoice, 1);
    }

    for (int i = 0; i < 12; i++)
    {
        if (i < 5)
        {
            free(gameAssets[i]);
        }
        free(trainImages[i]);
    }
    freePeripheralMemory();
    free(trainImages);
    free(gameAssets);
    return 0;
}