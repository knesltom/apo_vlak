
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <unistd.h>

#include "train_movement.h"
#include "draw_game.h"

// check for success in mallocs

int main()
{

    char **train_images = malloc(sizeof(char *) * 6);
    if (!train_images)
    {
        fprintf(stderr, "Error allocating memory for train images\n");
        exit(1);
    }
    for (int al = 0; al < 6; al++)
    {
        train_images[al] = malloc(sizeof(char) * 400);
        if (!train_images[al])
        {
            fprintf(stderr, "Error allocating memory for train images\n");
            exit(1);
        }
        load_train("train_assets.txt", train_images[al], al);
       }

    tile *board = malloc(BOARD_SIZE * sizeof(tile));

    train_s *train = (train_s *)malloc(sizeof(train_s));

    char *loot_image = malloc(sizeof(char) * 400);

    load_loot("loot.txt", loot_image);

    for (int k = 0; k < 400; k++)
    {
        printf("%c", loot_image[k]);
        if ((k + 1) % 20 == 0)
        {
            printf("\n");
        }
    }

    int exit_position = 0;
    exit_position = load_level(board, "level2.txt", train);

    create_train(train);
    int move_count = 0;
    draw_game(board, train, loot_image, train_images[RIGHT]);

    while (train->type != 'D')
    {

        for (int i = 0; i < BOARD_SIZE; i++)
        {
            bool wagon_printed = false;
            if (train->position == i)
            {
                fprintf(stderr, "%c", train->type);
                continue;
            }
            for (int j = 0; j < train->wagon_count; j++)
            {
                if (i == train->wagons[j]->position)
                {
                    fprintf(stderr, "%c", train->wagons[j]->type);
                    wagon_printed = true;
                }
            }

            if (!wagon_printed)
            {
                fprintf(stderr, "%c", board[i].type);
                wagon_printed = false;
            }
            if ((i + 1) % BOARD_WIDTH == 0)
            {
                printf("\n");
            }
        }
        // only for testing purposes
        if (move_count >= 4)
        {
            erase_train(train, board);
            move_down(train, board);
            redraw_train(train, board, train_images[DOWN_RIGHT_ROTATION]);
        }
        else
        {
            erase_train(train, board);
            move_right(train, board);
            redraw_train(train, board, train_images[RIGHT_ROTATION]);
        }

        move_count++;
        open_exit(board, exit_position);
        sleep((int)1);
    }

    // last print

    for (int i = 0; i < BOARD_SIZE; i++)
    {
        bool wagon_printed = false;
        if (train->position == i)
        {
            fprintf(stderr, "%c", train->type);
            continue;
        }
        for (int j = 0; j < train->wagon_count; j++)
        {
            if (i == train->wagons[j]->position)
            {
                fprintf(stderr, "%c", train->wagons[j]->type);
                wagon_printed = true;
            }
        }
        if (!wagon_printed)
        {
            fprintf(stderr, "%c", board[i].type);
        }

        if ((i + 1) % BOARD_WIDTH == 0)
        {
            printf("\n");
        }
    }
    free(board);
    free(train);
    // fclose(file);

    return 0;
}
