#include "levels.h"
// add parameter for level number, so that after level is finished, player can play the next one
void runLevel(int gate, tile *board, train_s *train, char **trainImages, char **assetImages, int levelNumber, uint32_t numberOfTries)
{
    *(volatile uint32_t *)(knob_mem_base + SPILED_REG_LED_LINE_o) = numberOfTries;
    drawBoard(board, train, assetImages, trainImages);
    while (1)
    {

        pthread_mutex_lock(&buttonMutex);
        int redPress = redButton;
        int bluePress = blueButton;
        pthread_mutex_unlock(&buttonMutex);
        if (redPress && bluePress)
        {
            while (1)
            {
                pthread_mutex_lock(&buttonMutex);
                redPress = redButton;
                bluePress = blueButton;
                pthread_mutex_unlock(&buttonMutex);
                if (!redPress && !bluePress)
                {
                    break;
                }
            }
            break;
        }
    }
    bool pauseGame = false;
    while (train->position != gate && train->type != 'D')
    {

        struct timespec gameSpeed = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
        struct timespec waitTime = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 10};
        if (greenButton)
        {
            if (!pauseGame)
            {
                pauseGame = true;
                printf("GAME PAUSED!\n");
                clock_nanosleep(CLOCK_MONOTONIC, 0, &gameSpeed, NULL);
            }
        }
        while (pauseGame)
        {
            if (redButton)
            {
                int levelPicked = -1;
                levelPicked = showLevels();
                if (levelPicked != -1)
                {
                    gate = loadLevelFromFile(board, "level_selection.txt", train, levelPicked);
                    return runLevel(gate, board, train, trainImages, assetImages, levelPicked, 1);
                }
            }
            else if (greenButton)
            {
                pauseGame = false;
            }
            else if (blueButton)
            {
                return;
            }

            clock_nanosleep(CLOCK_MONOTONIC, 0, &gameSpeed, NULL);
            continue;
        }

        if (redButton)
        {
            switch (train->trainDirection)
            {
            case RIGHT:
                moveUp(train, board, trainImages);
                break;
            case LEFT:
                moveDown(train, board, trainImages);
                break;
            case DOWN:
                moveRight(train, board, trainImages);
                break;
            case UP:
                moveLeft(train, board, trainImages);
                break;
            }
            clock_nanosleep(CLOCK_MONOTONIC, 0, &gameSpeed, NULL);
        }
        else if (blueButton)
        {
            switch (train->trainDirection)
            {
            case RIGHT:
                moveDown(train, board, trainImages);
                break;
            case LEFT:
                moveUp(train, board, trainImages);
                break;
            case DOWN:
                moveLeft(train, board, trainImages);
                break;
            case UP:
                moveRight(train, board, trainImages);
                break;
            }

            clock_nanosleep(CLOCK_MONOTONIC, 0, &gameSpeed, NULL);
        }
        else
        {
            switch (train->trainDirection)
            {
            case RIGHT:
                moveRight(train, board, trainImages);
                break;
            case LEFT:
                moveLeft(train, board, trainImages);
                break;
            case DOWN:
                moveDown(train, board, trainImages);
                break;
            case UP:
                moveUp(train, board, trainImages);
                break;
            }
            for (int i = 0; i < 100; i++)
            {
                clock_nanosleep(CLOCK_MONOTONIC, 0, &waitTime, NULL);
                if (greenButton)
                {
                    pauseGame = true;
                    clock_nanosleep(CLOCK_MONOTONIC, 0, &gameSpeed, NULL);
                    printf("GAME PAUSED!\n");
                    break;
                }
            }
        }
        openExit(board, gate);
        if (board[gate].type == 'O')
        {
            drawOpenGate(gate, assetImages);
        }
    }

    if (train->type == 'D')
    {
        numberOfTries += numberOfTries + 1;
        drawCrash(train, trainImages);
        while (1)
        {
            pthread_mutex_lock(&buttonMutex);
            int bluePress = blueButton;
            pthread_mutex_unlock(&buttonMutex);

            if (bluePress)
            {
                gate = loadLevelFromFile(board, "level_selection.txt", train, levelNumber);
                return runLevel(gate, board, train, trainImages, assetImages, levelNumber, numberOfTries);
            }
        }
    }
    gate = loadLevelFromFile(board, "level_selection.txt", train, levelNumber + 1);
    return runLevel(gate, board, train, trainImages, assetImages, levelNumber + 1, 1);
}

int playOpeningSequence(const char *fname, char **trainAssets, char **gameAssets, train_s *train, tile *board)
{
    loadLevelFromFile(board, fname, train, 0);

    drawBoard(board, train, gameAssets, trainAssets);
    // int redButton, greenButton, blueButton;
    // now run until interrupted by user pressing a button
    while (1)
    {
        if (blueButton)
        {
            // if player pressed blue button, optional (show train speed, hardcore mode selection),
            // then by pressing a button start level 1 and go from there
            return 1;
        }
        else if (greenButton)
        {
            // if green, display level names, and player can pick one by turning green knob and confirm it by pressing it,
            // then after finishing that level continue onward from that one
            return 0;
        }
        else if (redButton)
        {
            // if red, display credits and how to play (maybe),
            // then by pressing a button go back to main menu
            return -1;
        }
    }
}

int showLevels()
{
    int rKnob, gKnob, bKnob;
    int rButton, gButton, bButton;
    uint32_t levelChoice = 1;
    while (1)
    {
        struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
        getKnobRotation(&rKnob, &gKnob, &bKnob);
        getAllButtonsValues(&rButton, &gButton, &bButton);
        levelChoice = (bKnob / 4) % 3;
        *(volatile uint32_t *)(knob_mem_base + SPILED_REG_LED_LINE_o) = pow(2, levelChoice);
        if (rButton)
        {

            printf("Level number picked: %d\n", levelChoice + 1);
            clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
        }
        if (bButton && levelChoice < 4)
        {
            return levelChoice + 1;
        }
        else if (bButton && levelChoice >= 4)
        {
            fprintf(stderr, "INVALID LEVEL NUMBER!\n");
            clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
        }
    }
}
