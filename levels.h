#ifndef LEVELS_H
#define LEVELS_H

#include "train_movement.h"

/**
 * All levels will be read from appropriate files
 * The tiles are as follows:
 *      wall -> 'X'
 *      floor -> '.'
 *      stone -> 'S'
 *      fruit -> 'F'
 *      tree -> 'T'
 *      exit -> 'E'
 *      locomotive start position is first number in level file, the second is exit position
 */

void runLevel(int gate, tile *board, train_s *train, char **trainImages, char **lootImages, int levelNumber, uint32_t numberOfTries);

int playOpeningSequence(const char *fname, char **trainAssets, char **lootAssets, train_s *train, tile *board);

int showLevels();
#endif