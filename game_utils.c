#include "game_utils.h"

void setTiles(tile *tileArray)
{
    tile wall;
    tile floor;
    tile tree;
    tile stones;
    tile fruit;
    tile exit;

    wall.freeSpace = false;
    wall.type = 'X';

    floor.freeSpace = true;
    floor.type = '.';

    // tree.image = "tree.png";
    tree.freeSpace = true;
    tree.type = 'T';

    // stones.image = "stones.png";
    stones.freeSpace = true;
    stones.type = 'S';

    // fruit.image = "fruit.png";
    fruit.freeSpace = true;
    fruit.type = 'F';

    // exit.image = "exit.png";
    exit.freeSpace = false;
    exit.type = 'E';

    tileArray[0] = wall;
    tileArray[1] = floor;
    tileArray[2] = tree;
    tileArray[3] = stones;
    tileArray[4] = fruit;
    tileArray[5] = exit;
}

void openExit(tile *board, int gatePosition)
{
    int count = 0;
    for (int i = 0; i < BOARD_SIZE; i++)
    {
        switch (board[i].type)
        {
        case 'S':
            count++;
            break;
        case 'F':
            count++;
            break;
        case 'T':
            count++;
            break;
        case 'W':
            count++;
            break;
        }
    }
    if (count == 0)
    {
        board[gatePosition].freeSpace = true;
        board[gatePosition].type = 'O';
    }
}

void allocateTrain(train_s *train)
{
    train->wagons = (wagon_s **)malloc(sizeof(wagon_s *));
    train->wagons[0] = (wagon_s *)malloc(sizeof(wagon_s));
    train->wagonCount = 0;
    train->trainDirection = RIGHT;
    train->type = '@';

    // train->type->backgroundImage = "floor.png"
    // train->type->image = "train.png"
}

void clearTrain(train_s *train)
{
    train->type = '@';
    train->trainDirection = RIGHT;
    for (int i = 0; i < train->wagonCount; i++)
    {
        train->wagonCount = 0;

        free(train->wagons[i]);
    }
}

char **allocateAssetMemory(size_t arraySize, int numberOfArrays)
{
    char **arrayOfArrays = (char **)malloc(numberOfArrays * sizeof(char *));
    if (!arrayOfArrays)
    {
        fprintf(stderr, "Error allocating memory for array of arrays\n");
        return NULL;
    }
    for (int i = 0; i < numberOfArrays; i++)
    {
        arrayOfArrays[i] = (char *)malloc(arraySize * sizeof(char));
        if (!arrayOfArrays[i])
        {
            fprintf(stderr, "Error allocating memory for array of index %d\n", i);
            return NULL;
        }
    }
    return arrayOfArrays;
}

void readAssets(char **arrayOfArrays, const char *fname, int numberOfAssets)
{

    /**
     * W - WHITE, R - RED, G - GREY, B - BLUE, '.' - BLACK,
     *  T - TURQOISE, Z - GREEN, H - BROWN, M - BLUE, Y - YELLOW
     */
    char types[11] = {'W', 'R', 'G', 'B', '.', 'X', 'T', 'Z', 'H', 'M', 'Y'};
    FILE *fp = fopen(fname, "r");
    if (!fp)
    {
        fprintf(stderr, "Error opening file %s\n", fname);
        // clean memory
        exit(1);
    }

    char temp;
    char buffer[20];
    for (int i = 0; i < numberOfAssets; i++)
    {
        bool found = false;
        while (fgets(buffer, sizeof(buffer), fp))
        {
            if (atoi(buffer) == i)
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            int index = 0;
            while (index < BLOCK_SIZE)
            {
                temp = fgetc(fp);
                for (int j = 0; j < 11; j++)
                {
                    if (temp == types[j])
                    {
                        arrayOfArrays[i][index] = temp;
                        index++;
                        break;
                    }
                }
            }
        }
    }
    fclose(fp);
}

int loadLevelFromFile(tile *board, const char *levelSelection, train_s *train, int levelChoice)
{
    clearTrain(train);
    tile *tiles = malloc(6 * sizeof(tile));
    if (!tiles)
    {
        fprintf(stderr, "Error allocating memory!\n");
        return -1;
    }
    setTiles(tiles);

    FILE *level = fopen(levelSelection, "r");
    if (!level)
    {
        fprintf(stderr, "Error opening file!\n");
        return -1;
    }
    char buffer[24];

    bool found = false;

    while (fgets(buffer, sizeof(buffer), level))
    {
        if (atoi(buffer) == levelChoice)
        {
            found = true;
            break;
        }
    }

    int gatePosition;
    char temp;
    if (found)
    {
        int r = fscanf(level, "%d %d\n", &train->position, &gatePosition);
        if (r != 2)
        {
            fprintf(stderr, "Wrong level input format! Train start is :%d and exit is:%d\n", train->position, gatePosition);
            return -1;
        }
        int index = 0;
        while (index < BOARD_SIZE)
        {
            temp = fgetc(level);

            if (temp == 'X')
            {
                board[index++] = tiles[0];
            }
            else if (temp == '.')
            {
                board[index++] = tiles[1];
            }
            else if (temp == 'T')
            {
                board[index++] = tiles[2];
            }
            else if (temp == 'S')
            {
                board[index++] = tiles[3];
            }
            else if (temp == 'F')
            {
                board[index++] = tiles[4];
            }
            else if (temp == 'E')
            {
                board[index++] = tiles[5];
            }
        }
        printf("Train start is :%d and exit is:%d\n", train->position, gatePosition);
    }

    fclose(level);
    free(tiles);
    return gatePosition;
}
