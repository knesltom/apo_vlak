#include "peripherals.h"

unsigned short *fb;
unsigned char *parlcd_mem_base;
unsigned char *knob_mem_base;

bool initLCD()
{
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    fb = (unsigned short *)malloc(320 * 480 * 2);
    if (!fb)
    {
        return false;
    }

    if (!parlcd_mem_base)
    {
        return false;
    }

    unsigned int c;

    parlcd_hx8357_init(parlcd_mem_base);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    int ptr = 0;
    int i, j;
    for (i = 0; i < 320; i++)
    {
        for (j = 0; j < 480; j++)
        {
            c = 0;
            fb[ptr] = c;
            parlcd_write_data(parlcd_mem_base, fb[ptr++]);
        }
    }
    for (ptr = 0; ptr < 320 * 480; ptr++)
    {
        fb[ptr] = 0u;
    }
    return true;
}

bool initKNOBS()
{
    knob_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (!knob_mem_base)
    {
        return false;
    }

    return true;
}

void getAllButtonsValues(int *redButton, int *greenButton, int *blueButton)
{
    uint32_t rgbKnobsValue;
    rgbKnobsValue = *(volatile uint32_t *)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);

    *redButton = (rgbKnobsValue >> 26) & 1;
    *greenButton = (rgbKnobsValue >> 25) & 1;
    *blueButton = (rgbKnobsValue >> 24) & 1;
}

void getKnobRotation(int *redKnob, int *greenKnob, int *blueKnob)
{
    uint32_t rgbKnobsValue;
    rgbKnobsValue = *(volatile uint32_t *)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);

    *redKnob = (rgbKnobsValue >> 16) & 0xFF;
    *greenKnob = (rgbKnobsValue >> 8) & 0xFF;
    *blueKnob = (rgbKnobsValue) & 0xFF;
}

void *buttonPressThread(void *arg)
{
    while (1)
    {
        getAllButtonsValues(&redButton, &greenButton, &blueButton);
    }

    return NULL;
}

void freePeripheralMemory()
{
    free(fb);
    free(parlcd_mem_base);
    free(knob_mem_base);
}
