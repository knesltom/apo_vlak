#ifndef PERIPHERALS
#define PERIPHERALS

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <math.h>

extern int redButton;
extern int greenButton;
extern int blueButton;

extern pthread_mutex_t buttonMutex;

extern unsigned short *fb;
extern unsigned char *parlcd_mem_base;
extern unsigned char *knob_mem_base;

/* Call at the start of the program*/
bool initLCD();
/* Call at the start of the program*/
bool initLEDS();
/* Call at the start of the program*/
bool initKNOBS();

void getAllButtonsValues(int *redButton, int *greenButton, int *BlueButton);

void getKnobRotation(int *redKnob, int *greenKnob, int *blueKnob);

/* Call before end of main function*/
void freePeripheralMemory();

void *buttonPressThread(void *arg);

#endif
