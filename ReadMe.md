# Computer Architectures (B35APO)
## VLAK - MicroZed APO
###### Summer term 2024
*Authors: Kneslik Tomas, Vokal Jan*

This work is a remake of an old game [VLAK](https://cs.wikipedia.org/wiki/Vlak_(po%C4%8D%C3%ADta%C4%8Dov%C3%A1_hra)) for MS-DOS, specifically for MicroZed APO kit. The game might not be polished as of end of summer term, due to time constraints. 

### Rundown of game mechanics
It is a simple 2D arcade game, where the player navigates a train in a set of designed levels. The objective that player has to complete before entering next **scene** is to colect every item lying on the ground. For each item picked, one wagon will be added to the back, thus increasing the length of the train the player has to control. 

**Side notes**
- Every level has a set spawn points for train and items
- Maneuvering the train into a wall, closed gate or a wagon will result in crash and a restart of the current level
- Some levels might be impossible to pass after any mistake
- scene is the game's name for levels

### How to Play
Usually, the train is being controlled by **arrows**, or **W A S D**
If the player would wish to play a different **scene**, they would type a code (5 letters long), and confirm with enter.

On **MicroZed APO**, the train will be controlled by pressing (turning) the two knobs (R and B), while the middle knob will be used as a way of pausing the game and choosing a different level. (Unless we connnect a keyboard, then the problem of controlling the train is way more simple. :skull: **Without keyboard, turning the train by 180 degrees will be impossible**)
- LED diodes will be used to count the number of tries player has on a **scene**

### How to Build + side info
The game is written in C, use the given makefile for compilation. The game **cannot** be compiled on architectures other then ARM (I think :custard:)
The game is written in a way that it is easy to add new levels and items.
- In a different document [HERE] (insert link and delete space) will be described a way, how the assets are made (It should make a understandable guide on how to add more or change the existing ones)

#### Peripherals used (might edit later)
- Paralel LCD display
- (Knobs)/(Keyboard)
- LED diodes

### TL;DR
- Turn the knobs to turn the train (really simple, trust me)
- Don't crash
- Collect every item
- Escape through the gate when it's opened

(To the one person who read this :custard:)

*Thank you for reading :heart:* 