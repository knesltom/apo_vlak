#include "draw_game.h"

/*This function is called at the start of each level and will only draw the default state of the board*/
void drawBoard(tile *board, train_s *train, char **gameImages, char **trainImages)
{
    int i, j, k = 0;
    int ptr;
    bool repainted = false;
    int startX, endX, startY, endY;

    while (k < BOARD_SIZE)
    {
        startX = (20 * (k % 24)) % 480;
        endX = startX + 20;
        startY = (20 * (k / 24)) % 320;
        endY = startY + 20;

        endX = endX > 480 ? 480 : endX;
        endY = endY > 320 ? 320 : endY;

        if (board[k].type == 'X')
        {
            repainted = true;
            drawGameAsset(gameImages[4], startX, startY);
        }
        else if (board[k].type == 'E')
        {
            repainted = true;
            drawGameAsset(gameImages[3], startX, startY);
        }
        else if (board[k].type == 'F' || board[k].type == 'T' || board[k].type == 'S')
        {
            repainted = true;
            if (board[k].type == 'F')
            {
                drawGameAsset(gameImages[0], startX, startY);
            }
            else if (board[k].type == 'T')
            {
                drawGameAsset(gameImages[1], startX, startY);
            }
            else
            {
                drawGameAsset(gameImages[2], startX, startY);
            }
        }
        else if (train->position == k)
        {
            repainted = true;
            drawTrain(train, board, trainImages, RIGHT_ROTATION);
        }
        if (repainted)
        {
            repainted = false;
        }
        else
        {
            for (i = startY; i < endY; i++)
            {
                for (j = startX; j < endX; j++)
                {
                    fb[j + i * 480] = 0x0;
                }
            }
        }
        k++;
    }

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++)
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}

void drawCrash(train_s *train, char **trainImages)
{
    int ptr = 0;
    int startX = (20 * (train->position % 24)) % 480;
    int endX = startX + 20;
    int startY = (20 * (train->position / 24)) % 320;
    int endY = startY + 20;

    endX = endX > 480 ? 480 : endX;
    endY = endY > 320 ? 320 : endY;

    pickColors(trainImages[12], startX, startY);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++)
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
    pickColors(trainImages[13], startX, startY);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++)
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}

/**
 * Try to fix this function
 */
void drawTrain(train_s *train, tile *board, char **trainImages, int trainRotation)
{
    int i;
    int ptr = 0;

    int startX = (20 * (train->position % 24)) % 480;
    int endX = startX + 20;
    int startY = (20 * (train->position / 24)) % 320;
    int endY = startY + 20;

    endX = endX > 480 ? 480 : endX;
    endY = endY > 320 ? 320 : endY;

    pickColors(trainImages[trainRotation], startX, startY);

    if (train->wagonCount > 0)
    {
        for (i = train->wagonCount - 1; i >= 0; i--)
        {
            if (i == 0)
            {
                train->wagons[i]->wagonDirection = trainRotation + 6;
            }
            else
            {
                train->wagons[i]->wagonDirection = train->wagons[i - 1]->wagonDirection;
            }

            int wagon_startX = (20 * (train->wagons[i]->position % 24)) % 480;
            int wagon_startY = (20 * (train->wagons[i]->position / 24)) % 320;

            pickColors(trainImages[train->wagons[i]->wagonDirection], wagon_startX, wagon_startY);
        }
    }

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++)
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}

void drawOpenGate(int gate, char **gameImages)
{
    int ptr = 0;
    int startX = (20 * (gate % 24)) % 480;
    int endX = startX + 20;
    int startY = (20 * (gate / 24)) % 320;
    int endY = startY + 20;

    endX = endX > 480 ? 480 : endX;
    endY = endY > 320 ? 320 : endY;

    pickColors(gameImages[5], startX, startY);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++)
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}
/**
 * Erases current train from Display on its current position (Erase before movement, then redraw)
 */
void eraseTrain(train_s *train, tile *board)
{
    int i, j, k, l;
    int ptr = 0;

    int startX = (20 * (train->position % 24)) % 480;
    int endX = startX + 20;
    int startY = (20 * (train->position / 24)) % 320;
    int endY = startY + 20;

    endX = endX > 480 ? 480 : endX;
    endY = endY > 320 ? 320 : endY;

    for (i = startY; i < endY; i++)
    {
        for (j = startX; j < endX; j++)
        {
            fb[j + i * 480] = 0x0;
        }
    }

    if (train->wagonCount > 0)
    {
        for (i = 0; i < train->wagonCount; i++)
        {
            int wagon_startX = (20 * (train->wagons[i]->position % 24)) % 480;
            int wagon_endX = wagon_startX + 20;
            int wagon_startY = (20 * (train->wagons[i]->position / 24)) % 320;
            int wagon_endY = wagon_startY + 20;

            for (k = wagon_startY; k < wagon_endY; k++)
            {
                for (l = wagon_startX; l < wagon_endX; l++)
                {
                    fb[l + k * 480] = 0x0;
                }
            }
        }
    }

    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (ptr = 0; ptr < 480 * 320; ptr++)
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
    // Plus add erasing wagons from their previous positions
}

void drawGameAsset(char *gameImages, int startX, int startY)
{
    pickColors(gameImages, startX, startY);
}
// this function will replace draw loot and draw train when finished
void pickColors(char *asset, int startX, int startY)
{
    int i, j;
    int ptr = 0;

    for (i = startY; i < startY + 20; i++)
    {
        for (j = startX; j < startX + 20; j++)
        {
            bool painted = false;
            char color = asset[ptr++];
            switch (color)
            {
            case 'W': // WHITE
                painted = true;
                fb[j + i * 480] = 0xffff;
                break;
            case 'R': // RED
                painted = true;
                fb[j + i * 480] = 0xf800;
                break;
            case 'G': // GRAY
                painted = true;
                fb[j + i * 480] = 0x8c71;
                break;
            case 'B': // BLUE
                painted = true;
                fb[j + i * 480] = 0x002f;
                break;
            case 'H': // BROWN
                painted = true;
                fb[j + i * 480] = 0x8220;
                break;
            case 'Z': // GREEN
                painted = true;
                fb[j + i * 480] = 0x07e7;
                break;
            case 'T': // TURQUOISE
                painted = true;
                fb[j + i * 480] = 0x07fc;
                break;
            case 'Y': // YELLOW
                painted = true;
                fb[j + i * 480] = 0xff20;
                break;
            case 'M': // lighter blue
                painted = true;
                fb[j + i * 480] = 0x003e;
                break;
            default:
                break;
            }
            if (!painted)
            {
                fb[j + i * 480] = 0x00; // BLACK
            }
        }
    }
}
