

#ifndef GAME_UTILS
#define GAME_UTILS

#include "peripherals.h"

#define BOARD_SIZE 384
#define BLOCK_SIZE 400
#define BOARD_WIDTH 24
#define BOARD_HEIGHT 16
#define CHAR_ARRAY_SIZE 12

enum direction
{
    RIGHT = 1,
    DOWN = 2,
    LEFT = 3,
    UP = 4
};

enum trainDirection
{
    LEFT_ROTATION,
    RIGHT_ROTATION,
    UP_LEFT_ROTATION,
    UP_RIGHT_ROTATION,
    DOWN_LEFT_ROTATION,
    DOWN_RIGHT_ROTATION
};

typedef struct tile_s
{
    unsigned char *backgroundImage;
    unsigned char *image;
    bool freeSpace;
    char type;

} tile;

typedef struct wagon_t
{
    char type;
    int position;
    int wagonDirection;
    struct wagon_t *nextWagon;     // @-#-#-#-# -> this direction
    struct wagon_t *previousWagon; // @-#-#-#-# <- this direction
} wagon_s;

typedef struct train_t
{
    char type;
    int position;
    int wagonCount;
    wagon_s **wagons;
    int trainDirection;
} train_s;

void setTiles(tile *tileArray);

void openExit(tile *board, int gatePosition);

void allocateTrain(train_s *train);

void clearTrain(train_s *train);

char **allocateAssetMemory(size_t arraySize, int numberOfArrays);

void readAssets(char **ArrayOfArrays, const char *fname, int numberOfAssets);

int loadLevelFromFile(tile *array, const char *level_name, train_s *train, int levelChoice);

#endif