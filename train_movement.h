#ifndef TRAIN_MOVES
#define TRAIN_MOVES

#include "draw_game.h"

#define CRASH -1

void trainCrash(train_s *train);

bool checkFreeSpace(train_s *train, tile *board, int direction);

void pickItem(train_s *train);

void moveRight(train_s *train, tile *board, char **trainImages);
void moveLeft(train_s *train, tile *board, char **trainImages);
void moveUp(train_s *train, tile *board, char **trainImages);
void moveDown(train_s *train, tile *board, char **trainImages);

#endif