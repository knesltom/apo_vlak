
#include "train_movement.h"

void trainCrash(train_s *train)
{
    train->type = 'D';
}

/**
 * 1 -> RIGHT
 * 2 -> DOWN
 * 3 -> LEFT
 * 4 -> UP
 */
bool checkFreeSpace(train_s *train, tile *board, int direction)
{

    switch (direction)
    {
    case 1:
        if (!board[train->position + 1].freeSpace)
        {
            trainCrash(train);
            return false;
        }
        for (int i = 0; i < train->wagonCount; i++)
        {
            if (train->position + 1 == train->wagons[i]->position)
            {
                trainCrash(train);
                return false;
            }
        }
        break;
    case 2:
        if (!board[train->position + BOARD_WIDTH].freeSpace)
        {
            trainCrash(train);
            return false;
        }
        for (int i = 0; i < train->wagonCount; i++)
        {
            if (train->position + BOARD_WIDTH == train->wagons[i]->position)
            {
                trainCrash(train);
                return false;
            }
        }
        break;
    case 3:
        if (!board[train->position - 1].freeSpace)
        {
            trainCrash(train);
            return false;
        }
        for (int i = 0; i < train->wagonCount; i++)
        {
            if (train->position - 1 == train->wagons[i]->position)
            {
                trainCrash(train);
                return false;
            }
        }
        break;
    case 4:
        if (!board[train->position - BOARD_WIDTH].freeSpace)
        {
            trainCrash(train);
            return false;
        }
        for (int i = 0; i < train->wagonCount; i++)
        {
            if (train->position - BOARD_WIDTH == train->wagons[i]->position)
            {
                trainCrash(train);
                return false;
            }
        }
        break;

    default:
        break;
    }
    return true;
}

void pickItem(train_s *train)
{
    if (train->wagonCount == 0)
    {
        train->wagons[0] = (wagon_s *)malloc(sizeof(wagon_s));
        train->wagons[0]->type = 'H';
        train->wagons[0]->position = train->position;
    }
    else
    {

        wagon_s **temp = realloc(train->wagons, sizeof(wagon_s *) * (train->wagonCount + 1));
        if (!temp)
        {
            fprintf(stderr, "Allocation error!\n");
            exit(2);
        }
        train->wagons = temp;

        train->wagons[train->wagonCount] = (wagon_s *)malloc(sizeof(wagon_s));

        train->wagons[train->wagonCount]->type = 'H';
        train->wagons[train->wagonCount]->position = train->wagons[train->wagonCount - 1]->position;
    }
    train->wagonCount++;
}

void moveRight(train_s *train, tile *board, char **trainImages)
{

    if (!checkFreeSpace(train, board, RIGHT))
    {
        return;
    }

    if (board[train->position + 1].type != '.' && board[train->position + 1].type != 'O')
    {
        pickItem(train);
    }
    eraseTrain(train, board);

    if (train->wagonCount > 0)
    {
        for (int i = train->wagonCount - 1; i > 0; i--)
        {
            train->wagons[i]->position = train->wagons[i - 1]->position;
        }
        train->wagons[0]->position = train->position;
    }
    train->position++;
    board[train->position].type = '.';
    train->trainDirection = RIGHT;
    drawTrain(train, board, trainImages, RIGHT_ROTATION);
}

void moveLeft(train_s *train, tile *board, char **trainImages)
{
    if (!checkFreeSpace(train, board, LEFT))
    {
        return;
    }
    if (board[train->position - 1].type != '.' && board[train->position + 1].type != 'O')
    {
        pickItem(train);
    }
    eraseTrain(train, board);
    for (int i = train->wagonCount - 1; i > 0; i--)
    {
        train->wagons[i]->position = train->wagons[i - 1]->position;
    }
    train->wagons[0]->position = train->position;
    train->position--;
    board[train->position].type = '.';
    train->trainDirection = LEFT;
    drawTrain(train, board, trainImages, LEFT_ROTATION);
}

void moveUp(train_s *train, tile *board, char **trainImages)
{
    if (!checkFreeSpace(train, board, UP))
    {
        return;
    }

    if (board[train->position - BOARD_WIDTH].type != '.' && board[train->position + 1].type != 'O')
    {
        pickItem(train);
    }
    eraseTrain(train, board);
    for (int i = train->wagonCount - 1; i > 0; i--)
    {
        train->wagons[i]->position = train->wagons[i - 1]->position;
    }
    train->wagons[0]->position = train->position;
    train->position -= BOARD_WIDTH;
    board[train->position].type = '.';
    train->trainDirection = UP;
    train->trainDirection == RIGHT ? drawTrain(train, board, trainImages, UP_LEFT_ROTATION) : drawTrain(train, board, trainImages, UP_RIGHT_ROTATION);
}
void moveDown(train_s *train, tile *board, char **trainImages)
{
    if (!checkFreeSpace(train, board, DOWN))
    {
        return;
    }

    if (board[train->position + BOARD_WIDTH].type != '.' && board[train->position + 1].type != 'O')
    {
        pickItem(train);
    }
    eraseTrain(train, board);
    for (int i = train->wagonCount - 1; i > 0; i--)
    {
        train->wagons[i]->position = train->wagons[i - 1]->position;
    }
    train->wagons[0]->position = train->position;
    train->position += BOARD_WIDTH;
    board[train->position].type = '.';
    train->trainDirection = DOWN;
    train->trainDirection == RIGHT ? drawTrain(train, board, trainImages, DOWN_RIGHT_ROTATION) : drawTrain(train, board, trainImages, DOWN_LEFT_ROTATION);
}
